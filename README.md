# Recipe-app-api-proxy

NGINX  proxy app for our recipe  app API

##  usage

### Environment variables

* `LISTEN_PORT` - Port  to listen on (default: `8000`)
* `APP_HOST` - Hostname of the  app to forward the request to ( default:`app`)
* `APP_PORT` - port of the  app to forward the request to ( default:`9000`)
